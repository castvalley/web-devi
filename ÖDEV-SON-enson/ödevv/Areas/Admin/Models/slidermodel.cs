﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ödevv.Areas.Admin.Models
{
    public class slidermodel
    {
        public int ID { get; set; }
        public string slidertext { get; set; }
        public Nullable<System.DateTime> baslangictarihi { get; set; }
        public Nullable<System.DateTime> bitistarihi { get; set; }
        public HttpPostedFileBase sliderfoto { get; set; }

    }
}