﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ödevv.Areas.Admin.Models
{
    public class RolEkleModel
    {
        public string RolAd  { get; set; }

        //kullanici ekleme modelleri
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}
