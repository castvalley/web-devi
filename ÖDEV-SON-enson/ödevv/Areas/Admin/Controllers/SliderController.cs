﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ödevv.Areas.Admin.Models;

namespace ödevv.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {

        castvalleyEntities db= new castvalleyEntities();

        // GET: Admin/Slider
        public ActionResult slider()

        {
            var model = db.slider.OrderByDescending(x => x.ID).ToList();

            return View(model);
        }

        public ActionResult add()
        {
            
            return View();

        }

        const string ResimYolu = "/Content/img/";

        public ActionResult addslider(slidermodel model)
        {

            string fileName = string.Empty;
            
            if (ModelState.IsValid)
            {
                
           
                //dosyaya kaydetme

                if (model.sliderfoto != null && model.sliderfoto.ContentLength > 0)
                {
                    fileName = model.sliderfoto.FileName;
                    var path=Path.Combine( Server.MapPath(ResimYolu)  ,fileName);
                    model.sliderfoto.SaveAs(path);

                }
                
                
                //ef nesnesi olusturma
                slider slider=new slider();
                slider.baslangictarihi = model.baslangictarihi;
                slider.bitistarihi = model.bitistarihi;
                slider.slidertext = model.slidertext;
                slider.imagepath = ResimYolu+fileName;



                db.slider.Add(slider);
                db.SaveChanges();
            }
            return RedirectToAction("slider");



        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
          var model= db.slider.Find(id);

            //db.slider.FirstOrderDefault(x=>x.ID==id)

            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(slider model)
        {
            if(ModelState.IsValid)
            {
                

                var attached =db.slider.Attach(model);

                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            
             }    

           
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.slider.Find(id);

            //db.slider.FirstOrderDefault(x=>x.ID==id)

            return View(model);

        }

        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.slider.Find(id);
            db.slider.Remove(model);
            db.SaveChanges();

            return RedirectToAction("slider");


        }
    }
}