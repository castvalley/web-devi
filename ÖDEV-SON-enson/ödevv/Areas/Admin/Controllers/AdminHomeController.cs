﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ödevv.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
       
        [Authorize]
        // GET: Admin/Home
        public ActionResult index()
        {
            return View();
        }


        public ActionResult login()
        {
            return View();
        }

        public ActionResult tables()
        {
            return View();
        }

        public ActionResult user()
        {
            return View();
        }
    }
}