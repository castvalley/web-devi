﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ödevv.Areas.Admin.Models;
using ödevv.Models;

namespace ödevv.Areas.Admin.Controllers
{
    public class RolyonetimController : Controller
    {

        ApplicationDbContext context =new ApplicationDbContext();
        // GET: Admin/Rolyonetim
        public ActionResult Index()
        {

            var roleStore = new RoleStore<IdentityRole>();
            var roleManager =new RoleManager<IdentityRole>(roleStore);

            var model =roleManager.Roles.ToList();
           
            return View(model);
        }

        public ActionResult Rolekle()
        {


            return View("");
 }

        [HttpPost]
        public ActionResult Rolekle(RolEkleModel rol)
        {

            var roleStore = new RoleStore<IdentityRole>();
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (roleManager.RoleExists(rol.RolAd) == false)
            {
                roleManager.Create(new IdentityRole(rol.RolAd));

            }

            return RedirectToAction("Index");
        }

        public ActionResult RoleKullaniciEkle()
        {


            return View();
        }


        [HttpPost]
        public ActionResult RoleKullaniciEkle(RolEkleModel model)
        {

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore =new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.KullaniciAdi);


          

            if (!userManager.IsInRole(kullanici.Id, model.RolAdi))
            {
                userManager.AddToRole(kullanici.Id, model.RolAdi);

            }

            return RedirectToAction("Index");
        }
    }
}