﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ödevv.Controllers
{
    public class HomeController : Controller
    {

        castvalleyEntities db = new castvalleyEntities();

 


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Hakkimizda()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Iletisim()
            
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Giris()
        {

            return View();


        }

        public ActionResult Blog()
        {
            return View();


        }

        [ChildActionOnly]
        public ActionResult sliderview()
        {

            var liste= db.slider.Where(x => x.baslangictarihi < DateTime.Now && x.bitistarihi > DateTime.Now).OrderByDescending(x => x.ID);
             
            return View(liste);


        }
    }
}